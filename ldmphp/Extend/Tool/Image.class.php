<?php
if(!defined('LDM_PATH')) die('permession deny');
/* LDM 框架 图像处理类
* @Author: ldm
* @Date:   2014-04-29 10:18:13
* @Last Modified by:   dm
* @Last Modified time: 2014-04-29 16:39:04
*/
class Image{
	private $img;//当前图像资源
	public function __construct(){
		if(!extension_loaded('GD')){
			error('请开启gd图像库');
		}
	}

	/**
	 * [thumb 缩略图]
	 * @param  [type] $dFile     [原图]
	 * @param  [type] $outName   [缩略图文件名，没有后缀]
	 * @param  [type] $outPath   [缩略图文件路径]
	 * @param  [type] $thumbW    [缩略图宽]
	 * @param  [type] $thumbH    [缩略图高]
	 * @param  [type] $thumbType [生成缩略图类型]
	 * @return [type]            [缩略图文件路径，相对于根目录]
	 */
	public function thumb($dFile,$outName=null,$outPath=null,$thumbW=null,$thumbH=null,$thumbType=null){
		// 没开启缩略图
		if(!C('THUMB_ON')) return false;
		$thumbW = is_null($thumbW) ? C('THUMB_WIDTH') : $thumbW;	//缩略图宽度
		$thumbH = is_null($thumbH) ? C('THUMB_HEIGHT') : $thumbH;	//缩略图高度
		$thumbType = is_null($thumbType) ? C('THUMB_TYPE') : $thumbType;	//缩略图高度
		
		$info = getimagesize($dFile);	//目标图片信息
		$dType = trim(strrchr($info['mime'], '/'),'/');	//目标图片类型
		$pathInfo = pathInfo($dFile);
		$dVar = 'imagecreatefrom'.$dType;	//原图类型
		
		$name = $pathInfo['filename'];
		$outName = is_null($outName) ? $name . '_thumb.' . $dType : $outName . $dType;	//输出的文件名，包含后缀
		$outPath = is_null($outPath) ? C('THUMB_PATH') : $outPath;	//缩略图输出路径
		$outPath = rtrim($outPath,'/') . '/';	//缩略图输出路径
		is_dir($outPath) || mkdir($outPath,0777,true);	//保证输出目录存在
		$outFile = $outPath . $outName;		//缩略图输出文件
		$outStr = str_replace(ROOT_PATH, '', $outFile);
		// echo $outStr;die;
		$img = $dVar($dFile);	//创建目标图像资源
		$d = imagecreatetruecolor($thumbW, $thumbH);	//创建缩略图画布


		// 进行缩略图操作
		if(function_exists('imagecopyresampled')){
			imagecopyresampled($d, $img, 0, 0, 0, 0, $thumbW, $thumbH, imagesx($img), imagesy($img));
		}else{
			imagecopyresized($d, $img, 0, 0, 0, 0, $thumbW, $thumbH, imagesx($img), imagesy($img));
		}
		
		$func = 'image' . $dType;
		$func($d,$outFile);

		return $outStr;

	}

	/**
	 * [water 图片添加水印 - 默认如果水印宽或高大于原图，则不进行添加]
	 * @param  [type]  $dFile [目标文件- 添加水印的文件]
	 * @param  [type]  $sFile [源文件 - 水印文件 ]
	 * @param  integer $pos   [水印位置 1 左上角 2 右上角 3 左下角 4 右下角]
	 * @param  integer $pct   [透明度 默认80]
	 * @return [void]         [description]
	 */
	public function water($dFile,$sFile=null,$pos=null,$pct=null){
		// 没开启水印，返回
		if(!C('WATER_ON')) return false;
		$pct = is_null($pct) ? C('WATER_PCT') : $pct;	//透明度
		$pos = is_null($pos) ? C('WATER_POS') : $pos;	//透明度
		$sFile = is_null($sFile) ? C('WATER_PIC') : $sFile;	// 水印文件

		$desInfo = getimagesize($dFile);	//目标图片信息
		$sourInfo = getimagesize($sFile);	//目标图片信息

		$dType = trim(strrchr($desInfo['mime'], '/'),'/');	//目标图片类型
		$sType = trim(strrchr($sourInfo['mime'], '/'),'/');	//目标图片类型
		$dVar = 'imagecreatefrom'.$dType;
		$sVar = 'imagecreatefrom'.$sType;
		$d = $dVar($dFile);	//创建目标图像资源
		$s = $sVar($sFile);	//创建水印图片资源

		// 目标图尺寸
		$dX = imagesx($d);
		$dY = imagesy($d);
		// 水印尺寸
		$sX = imagesx($s);
		$sY = imagesy($s);

		// 默认如果水印宽或高大于原图，则不进行水印添加
		if($sX > $dX || $sY > $dY){
			return;
		}
		

		// 计算水印位置
		switch ($pos) {
			case 1://左上角
				$x = ($dX - $sX) > 10 ? 10 : ($dX - $sX);
				$y = ($dY - $sY) > 10 ? 10 : ($dY - $sY);
				break;
			case 2://右上角
				$x = ($dX - $sX - 10) > 0 ? ($dX - $sX - 10) : $dX - $sX;
				$y = ($dY - $sY) > 10 ? 10 : ($dY - $sY);
				break;
			case 3://左下角
				$x = ($dX - $sX) > 10 ? 10 : ($dX - $sX);
				$y = ($dY - $sY - 10 ) > 0 ? ($dY - $sY - 10 ) : $dY - $sY;
				break;
			case 4://右下角
				$x = ($dX - $sX - 10) > 0 ? ($dX - $sX - 10) : $dX - $sX;
				$y = ($dY - $sY - 10 ) > 0 ? ($dY - $sY - 10 ) : $dY - $sY;
				break;
			default:
				break;
		}

		// 添加水印
		imagecopymerge($d, $s, $x, $y, 0, 0, $sX, $sY, $pct);
		// 生成水印图片
		$img = 'image' . $dType;
		$img($d,$dFile);
	}
}
?>
