<?php
/* 
* 文章管理类
* @Author: dm
* @Date:   2014-04-22 09:35:11
* @Last Modified by:   dm
* @Last Modified time: 2014-04-22 10:46:15
*/
class Article{
	private $db;
	public function __construct(){
		$this->db = new Db();
	}
	//文章首页
	function index(){
		$count = $this->db->count();
		$page = new Page($count,2);
		$list = $this->db->limit($page->limit())->select();

		include(PATH_ROOT.'/template/index.html');
	}
	/**
	 * 显示文章
	 * @return [type] [description]
	 */
	function show(){
		$id = @$_GET['id'];
		if(!$id){
			error();
		}
		$data = $this->db->one($id);
		include(PATH_ROOT.'/template/show.html');

	}
	/**
	 * 添加文章
	 */
	function add(){
		if(IS_POST){
			$post = $_POST;
			$data['title'] = $post['title'];
			$data['content'] = $post['content'];
			$data['addtime'] = time();
			if($this->db->add($data)){
				success('添加文章成功！');
			}else{
				error('添加文章失败！');
			}
		}else{
			include(PATH_ROOT.'/template/addShow.html');
		}
		
	}
	//编辑文章
	function edit(){
		if(IS_POST){//提交编辑
			$post = $_POST;
			$data['title'] = $post['title'];
			$data['content'] = $post['content'];
			$data['addtime'] = time();
			$data['id'] = $post['id'];
			if($this->db->update($data)){
				success('编辑文章成功！');
			}else{
				error('编辑文章失败！');
			}
		}else{//显示编辑页面
			$id = @$_GET['id'];
			if(!$id){
				error();
			}
			$data = $this->db->one($id);
			include(PATH_ROOT.'/template/editShow.html');
		}
	}

	/**
	 * 删除
	 * @return [type] [description]
	 */
	function del(){
		$id = @$_GET['id'];
		if(!$id){
			error();
		}
		if($this->db->del($id)){
			success('删除文章成功！');
		}else{
			error('删除文章失败！');
		}
	}

	/**
	 * 不存在方法，404
	 * @return [type] [description]
	 */
	function error(){
			include(PATH_ROOT.'/template/404.html');
			exit;
	}
}


?>
