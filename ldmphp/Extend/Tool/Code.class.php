<?php
if(!defined('LDM_PATH')) die;
/* 验证码类
* @Author: ldm
* @Date:   2014-04-28 00:22:46
* @Last Modified by:   dm
* @Last Modified time: 2014-04-29 09:03:31
*/

class Code{
	private $img;//图像资源
	private $code;//验证码字符串
	private $len;//验证码字符个数
	private $width;//验证码长度
	private $height;//验证码高度
	private $bgcolor;//验证码背景色
	private $fontSize;//字体大小
	private $fontColor;//字体颜色
	private $fontFile;//字体文件
	private $seek;	//验证码种子
	function __construct($len=null,$width=null,$height=null,$bgColor=null,$fontSize=null,$fontColor=null,$fontFile=null,$seek=null){
		$this->fontFile = is_null($fontFile) ? C('CODE_FONT_FILE') : $fontFile;
		$this->fontFile = empty($this->fontFile) ? DATA_PATH . 'Font/font.ttf' : $this->fontFile;
		if(!is_file($this->fontFile)){
			error('验证码字体文件不存在');
		}
		$this->len = is_null($len) ? C('CODE_LEN') : $len;
		$this->width = is_null($width) ? C('CODE_WIDTH') : $width;
		$this->height = is_null($height) ? C('CODE_HEIGHT') : $height;
		$this->bgColor = is_null($bgColor) ? C('CODE_BG') : $bgColor;
		$this->fontSize = is_null($fontSize) ? C('CODE_FONT_SIZE') : $fontSize;
		$this->fontColor = is_null($fontColor) ? C('CODE_FONT_COLOR') : $fontColor;
		$this->seek = is_null($seek) ? C('CODE_SEEK') : $seek;
		
		
	}

	/**
	 * [show 显示验证码]
	 * @return [type] [description]
	 */
	public function show(){
		$this->_create_board();	//创建画布
		$this->_create_line();	//画线
		$this->_create_pixel();	//画点
		$this->_create_text();	//写字
		header("Content-Type:image/png");
		imagepng($this->img);
		imagedestroy($this->img);
	}
	/**
	 * [get_code 获得当前验证码字符串]
	 * @return [type] [description]
	 */
	public function get_code(){
		return $this->code;
	}
	/**
	 * [_create_font 写字]
	 * @return [type] [description]
	 */
	private function _create_text(){
		$str = '';
		for($i=0;$i<$this->len;$i++){
			$font = $this->seek;
			$font = $font[mt_rand(0,strlen($font)-1)]; //随机字符
			$str .= $font;
			// $x 
			$x = $this->width / $this->len;
			$x = $x * $i + 10;
			// $y
			$y = ($this->height + $this->fontSize) / 2;
			// 颜色
			$color = $this->fontColor;
			$color = imagecolorallocate($this->img, hexdec(substr($color,1,2)), hexdec(substr($color,3,2)), hexdec(substr($color,5,2)));
			imagettftext($this->img, $this->fontSize, mt_rand(-45,45), $x, $y, $color, $this->fontFile, $font);
		}
		// 获得验证码字符串
		$this->code = $str;
		$_SESSION['code'] = $this->code;
	}
	/**
	 * [_create_pixel 画点]
	 * @return [type] [description]
	 */
	private function _create_pixel(){
		for($i=0;$i<40;$i++){
			$color = imagecolorallocate($this->img, mt_rand(0,255), mt_rand(0,255), mt_rand(0,255));
			imagesetpixel($this->img, mt_rand(0,$this->width), mt_rand(0,$this->height), $color);
		}
	}
	/**
	 * [_create_line 画线]
	 * @return [type] [description]
	 */
	private function _create_line(){
		for($i=0;$i<8;$i++){
			$color = imagecolorallocate($this->img, mt_rand(0,255), mt_rand(0,255), mt_rand(0,255));
			imageline($this->img, mt_rand(0,$this->width), mt_rand(0,$this->height), mt_rand(0,$this->width), mt_rand(0,$this->height), $color);
		}

	}
	/**
	 * [_create_board 创建画布]
	 * @return [type] [description]
	 */
	private function _create_board(){
		$this->img = imagecreatetruecolor($this->width, $this->height);	//创建画布
		$color = $this->bgColor;
		$color = imagecolorallocate($this->img, hexdec(substr($color,1,2)), hexdec(substr($color,3,2)), hexdec(substr($color,5,2)));
		imagefill($this->img, 0, 0, $color);
	}
}
?>
