<?php
/* 
  操作数据
* @Author: dm
* @Date:   2014-04-22 08:50:36
* @Last Modified by:   dm
* @Last Modified time: 2014-04-22 10:28:06
*/
class Db{
	public $db;//当前数据表数据
	public $table;//模拟的数据表，这里用文件名代替
	public $path;//数据存放路径,
	public function __construct($dbname='',$path='./db'){
		$name = !empty($dbname)?$dbname:'db';
		$this->table = $name.".php";//数据库名称
		$dbPath = defined('PATH_CORE')?PATH_CORE:dirname(__FILE__);//当前文件目录
		$this->path = $dbPath.'/'.trim($path,'/');//数据库路径
		is_dir($this->path) || mkdir($this->path,0777,true);
		$this->table = $this->path.'/'.$this->table;// 初始化数据表
		if(!is_file($this->table)){
			file_put_contents($this->table, "<?php \r\nreturn array();\r\n?>");
		}
		// 获得当前数据表数据
		$this->db = (array)include($this->table);
	}
	public function count(){
		return count($this->db);
	}
	/**
	 * [limit ]
	 * @param  [type] $start [description]
	 * @param  string $end   [description]
	 * @return [type]        [description]
	 */
	public function limit($start){
		if(!$start) return $this;
		$arr = array();//返回数组
		$data = $this->db;
		// 只有一个参数
		if(!strpos($start,',')){
			for($i=0;$i<(int)$start;$i++){
				array_unshift($arr, array_shift($this->db));
			}
			
		}else{
			$lArr = explode(',',$start);
			$b = $lArr[1] - $lArr[0];
			// var_dump($this->db);
			// var_dump($lArr);
			
			// var_dump($lArr[0]-1);
			// var_dump((int)$lArr[0]+$b);
			// die;
			// 两个参数
			for($i=(int)$lArr[0]-1;$i<(int)$lArr[0]+$b;$i++){
				array_unshift($arr, $data[$i]);
			}
		}
		$this->db = $arr;
		return $this;

	}

	/**
	 * [one 获得一条记录]
	 * @return [type] [description]
	 */
	public function one($id){
		$db = $this->db;
		foreach($db as $k=>$v){
			if((int)$v['id'] == $id){
				return $v;
			}
		}
		return false;
	}
	/**
	 * [select 获得数据]
	 * @return [type] [description]
	 */
	public function select(){
		return $this->db;
	}
	/**
	 * [all 同select]
	 * @return [type] [description]
	 */
	public function all(){
		return $this->select();
	}

	/**
	 * [insert 插入记录]
	 * @param  [type] $data [插入的数据]
	 * @return [type]       [是否插入成功]
	 */
	public function insert($data){
		if(!is_array($data)) return false;
		$data = $this->setId($data);
		
		$newData = array_merge($this->db,array($data));//组合数据
		// var_dump($newData);

		$newData = $this->specialChars($newData);//HTML实体的处理


		$insert = var_export($newData,true);
		
		// 数据写入文件
		return file_put_contents($this->table, "<?php\r\nreturn ".$insert.";\r\n?>");
	}
	/**
	 * 添加数据 同insert
	 * @param [type] $data [description]
	 */
	public function add($data){
		return $this->insert($data);
	}
	/**
	 * [del 删除指定id数据]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function del($id){
		$db = $this->db;
		// var_dump($db);
		// unset($db[9]);
		// var_dump($db);
		// die;
		foreach($db as $k=>$v){
			if((int)$v['id'] == $id){
				unset($db[$k]);
				
				$this->db = $db;
				sort($this->db);

				$this->update();//更新当前数据表
				return true;
			}
		}
		return false;
	}
	public function update($data=''){
		if(!$data){
			$insert = var_export($this->db,true);
			
			// 数据写入文件
			$re =  file_put_contents($this->table, "<?php\r\nreturn ".$insert.";\r\n?>");
			// var_dump($re);die;
			return $re;
		}
		if(!is_array($data)) return false;

		$db = $this->db;
		foreach($db as $k=>$v){
			if($v['id'] == $data['id']){
				$db[$k] = $data;
				$this->db = $db;
				return $this->update();
			}
		}

		return false;

	}
	/**
	 * [delete 删除指定id数据 同 del]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function delete($id){
		$this->del($id);
	}
	/**
	 * [getId 获得唯一的数组下标]
	 * @param  [number] $count [数组下标序号]
	 * @return [number]        [唯一数组下标序号]
	 */
	private function setId($data){
		$db = $this->db;
		$ids = array();
		foreach ($db as $v) {
			if(isset($v['id'])){
				$ids[$v['id']] = $v['id'];
			}
		}
		ksort($ids);
		$data['id'] = end($ids) + 1;
		return $data;
	}

	/**
	 * html实体处理
	 * return 实体数据
	 */
	private function specialChars($arr){
		
		if(is_array($arr)){
			foreach ($arr as $k=>$v) {
				$arr[$k] = $this->specialChars($v);
			}
			return $arr;
		}else{
			return htmlspecialchars($arr);
		}

	}
}
?>
