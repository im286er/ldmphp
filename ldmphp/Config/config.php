<?php
if(!defined('LDM_PATH')) die('No access allowed');
/* ldm框架 配置文件
* @Author: ldm
* @Date:   2014-04-28 00:54:23
* @Last Modified by:   ldm
* @Last Modified time: 2014-05-11 11:28:52
*/
return array(

	'AUTO_LOAD_FILE'			=> 'ldm.php',						//自动加载文件列表，用半角,隔开
	/*系统调试*****************************/
	'DEBUG'						=> 0,						//开启调试模式
	'ERROR_TPL'					=> LDM_TPL_PATH . 'halt.html',	//错误信息模板
	'NOTICE_TPL'				=> LDM_TPL_PATH . 'notice.html',//提示性错误信息模版
	
	/*Log 日志处理*************************/
	'LOG_ON'					=> 1,						//记录日志
	'LOG_KEY'					=> 'ldmphp',				//日志加密字符串
	'LOG_SIZE'					=> 1000000,					//日志文件大小限制
	'LOG_TYPE'					=> array('error','notice','sql','warming'),	//保存日志的类型
	

	/*路由方式*****************************/
	'URL_TYPE'					=> 1,						//URL路由方式 1 pathinfo  2 普通 
	'PATHINFO_DLI'				=> '/',						//pathinfo 分隔符
	'PATHINFO_HTML'				=> '.html',					//伪静态后缀
	/*url变量******************************/
	'VAR_CONTROL'				=> 'c',						//模块变量
	'VAR_ACTION'				=> 'a',						//动作变量
	/*项目参数******************************/
	'DEFAULT_CONTEOL'			=> 'index',					//m默认控制器
	'DEFAULT_ACTION'			=> 'index',					//默认方法
	/*session******************************/
	'SESSION_START'				=> 1,						//是否自动开启session

	/*验证码********************************/
	'CODE_LEN'					=> 4,						//验证码个数
	'CODE_WIDTH'				=> 80,						//验证码长度
	'CODE_HEIGHT'				=> 20,						//验证码高度
	'CODE_BG'					=> '#dddddd',				//验证码背景色
	'CODE_FONT_SIZE'			=> 12,						//字体大小
	'CODE_FONT_COLOR'			=> '#333333',				//验证码颜色
	'CODE_FONT_FILE'			=> '',						//验证码字体
	'CODE_SEEK'					=> 'qwertyupasdfghjkzxcvbnm23456789',						//验证码种子


	// 模板引擎
	'TPL_EXT'					=> '.html',					//模板文件后缀名

	/*图像处理设置****************************/
	'WATER_ON'					=> 1,						//开启水印
	'WATER_PIC'					=> LDM_DATA_PATH . 'Image/ldm_water.png',	//水印文件
	'WATER_POS'					=> 4,						//水印位置 1 左上角 2 右上角 3 左下角 4 右下角
	'WATER_PCT'					=> 80,						//水印透明度
	'THUMB_ON'					=> 1,						//开启缩略图处理
	'THUMB_WIDTH'				=> 200,						//缩略图宽度
	'THUMB_HEIGHT'				=> 200,						//缩略图高度
	'THUMB_TYPE'				=> 1,						//缩略图类型
	'THUMB_PATH'				=> PUBLIC_PATH . 'Uploads/Image/' . date('Ym').'/',	//缩略图上传目录

	/*上传设置***************************/
	'UPLOAD_PATH'				=> PUBLIC_PATH . 'Uploads/' .  date('Ym').'/',	//文件上传目录
	);