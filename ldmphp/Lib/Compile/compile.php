<?php
if(!defined("LDM_PATH")) die('No access allowed');
/* LDM 框架 返回核心编译文件
* @Author: ldm
* @Date:   2014-04-30 00:52:09
* @Last Modified by:   ldm
* @Last Modified time: 2014-05-11 12:08:34
*/
// 返回核心编译文件
return array(
	LDM_FUNC_PATH . 'functions.php',
	LDM_CORE_PATH . 'App' . EXT,
	LDM_CORE_PATH . 'Control' . EXT,
	LDM_CORE_PATH . 'Route' . EXT,
	LDM_CORE_PATH . 'Log' . EXT,
	);