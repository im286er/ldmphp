<?php
/* LDM 框架路由类
* @Author: dm
* @Date:   2014-04-30 09:50:53
* @Last Modified by:   ldm
* @Last Modified time: 2014-05-11 16:12:20
*/
final class Route{

	public static function app(){
		// 请求内容
		$query = C('URL_TYPE') == 1 && isset($_SERVER['PATH_INFO']) ?  $_SERVER['PATH_INFO'] : $_SERVER['QUERY_STRING'];

		// 分析路由，并清楚伪静态后缀
		$query = self::_parse_route(str_ireplace(C('PATHINFO_HTML'), '', trim($query,'/')));

		// 拆分后的GET变量
		$_get = '';
		if(C('URL_TYPE') == 1 && isset($_SERVER['PATH_INFO'])){//pathinfo模式
			$query = str_ireplace(array('&','='), C('PATHINFO_DLI'), $query);


		}else{//普通get
			parse_str($query,$_get);
			array_merge($_GET,$_get);
		}
		// pathinfo 模式 获得参数数组表示
		$args = $_get || empty($query) ? array() : explode(C('PATHINFO_DLI'), trim($query,'/'));

		// 定义当前控制器
		if(C('URL_TYPE') == 2 && isset($_GET[C('VAR_CONTROL')])){// 默认路由传参
		}elseif(C('URL_TYPE') == 1 && isset($args[0]) && !empty($args[0])){// pathinfo模式
				// 第一个参数为模块变量，进行
				if($args[0] == C('VAR_CONTROL') && count($args) > 1){
					$_GET[C('VAR_CONTROL')] = $args[1];	
					array_shift($args);
					array_shift($args);
				}else{
					$_GET[C('VAR_CONTROL')] = $args[0];
					array_shift($args);
				}

		}else{//没有控制器传参，使用默认控制器
			$_GET[C('VAR_CONTROL')] = C('DEFAULT_CONTEOL');
		}

		// 定义当前方法
		if(C('URL_TYPE') == 2 && isset($_GET[C('VAR_ACTION')])){// 默认路由传参
		}elseif(C('URL_TYPE') == 1 && isset($args[0]) && !empty($args[0])){//pathinfo模式
			if($args[0] == C('VAR_ACTION') && count($args) > 1){
				$_GET[C('VAR_ACTION')] = $args[1];
				array_shift($args);
				array_shift($args);
			}else{
				$_GET[C('VAR_ACTION')] = $args[0];
				array_shift($args);
			}
		}else{//默认方法
			$_GET[C('VAR_ACTION')] = C('DEFAULT_ACTION');
		}


		// 获得get数据
		if(!empty($args)){
			$count = count($args);
			for($i=0;$i<$count;){
				$_GET[$args[$i]] = isset($args[$i+1]) ? $args[$i+1] : '';
				$i += 2;
			}
		}



		// 设置常量
		self::_set_const();

		// var_dump($_GET);
		// var_dump($args);
		// die;
				
	}

	/**
	 * [_parse_route 分析路由，用于路由规则]
	 * @param  [type] $query [请求内容]
	 * @return [type]        [description]
	 */
	private static function _parse_route($query){
		// 暂未开放功能
		return $query;
	}

	/**
	 * [_set_const 设置常量]
	 */
	private static function _set_const(){
		// 定义网址常量***************
		$host = 'http://'.rtrim($_SERVER['HTTP_HOST'],'/');//主机名
		define('__HOST__', $host);
		//网站根路径
		$docRoot = str_ireplace($_SERVER['DOCUMENT_ROOT'], '', dirname($_SERVER['SCRIPT_FILENAME'])).'/';
		$root = empty($docRoot) ? '' : '/' . trim(str_replace('\\', '/', $docRoot),'/');
		
		//网站根地址，不包含文件名
		define('__ROOT__',$host.$root);	
		
		// 网站根，含入口文件
		define("__WEB__", __HOST__ . $_SERVER['SCRIPT_NAME']);

		// 完整url
		define("__URL__", __HOST__ . '/' . trim($_SERVER['REQUEST_URI'],'/'));
		
		//网站 公共文件地址
		define('__PUBLIC__',__ROOT__ . '/Public/');	

		// 框架相关url************************
		// 框架根url
		define("__LDMPHP__", __HOST__ . '/' .str_ireplace(str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']), '',LDM_PATH));
		// 框架data文件目录url
		define("__LDMPHP_DATA_", __LDMPHP__ . 'Data');
		// 框架扩展目录url
		define("__LDMPHP_EXTEND__", __LDMPHP__ . 'Extend');
		// 框架模板目录url
		define("__LDMPHP_TPL__", __LDMPHP__ . 'Lib/Tpl/');

		/***********************************/ 
		// 当前控制器常量/
		define("CONTROL", $_GET[C('VAR_CONTROL')]);
		// 当前方法常量
		define("ACTION", $_GET[C('VAR_ACTION')]);

		switch (C('URL_TYPE')) {
			case 1://pathinfo 模式
				// 应用url
				define("__APP__", __WEB__);
				// 控制器url
				define("__CONTROL__", __APP__ . '/' . CONTROL);
				// 方法url
				define("__ACTION__", __CONTROL__ . '/' . ACTION);
				break;
			case 2:
				define("__APP__", __WEB__);
				define("__CONTROL__", __WEB__ . '?' . C('VAR_CONTROL') . '=' . CONTROL);
				define("__ACTION__", __CONTROL__ . "&" . C('VAR_ACTION') . '=' . ACTION);
			break;
		}

		/*网站应用url目录****************************/
		// 应用tpl url
		define('__TPL__',  __ROOT__ . '/' . str_ireplace(ROOT_PATH,'',str_replace('\\', '/', realpath(APP_TPL))) . '/'  );
		// 应用公共目录url 
		define('__TPL_PUBLIC__', __TPL__ . 'Public/');

			

	}

}