<?php
/* 
* @Author: dm
* @Date:   2014-04-28 14:24:15
* @Last Modified by:   dm
* @Last Modified time: 2014-04-29 09:48:06
*/
class Control{
	private $variables = array();
	/**
	 * [code 显示验证码]
	 * @return [type] [description]
	 */
	function code(){

		$code = new Code();
		$code->show();	
	}
	/**
	 * [error 错误提示]
	 * @return [type] [description]
	 */
	public function success($msg='操作成功',$url=null){
		header("Content-Type:text/html;charset=utf-8");
		$url = $url?$url:__ROOT__;
		echo "<script>alert('".$msg."');location.href='".$url."';</script>";
		exit;
	}
	/**
	 * [error 错误提示]
	 * @return [type] [description]
	 */
	public function error($msg='操作失败',$url=null){
		header("Content-Type:text/html;charset=utf-8");
		$url = $url?$url:__ROOT__;
		echo "<script>alert('".$msg."');location.href='".$url."';</script>";
		exit;
	}

	/**
	 * [display 模板显示]
	 * @return [type] [description]
	 */
	public function display($name=null){
		if(is_null($name)){//无参数，载入模板模板文件
			$tplFile = APP_TPL . CONTROL . '/' . ACTION . C('TPL_EXT');

		}
		if($name){
			
		}

		// 载入模板文件
		if(is_file($tplFile)){
			extract($this->variables);
			include $tplFile;
		}

	}

	/**
	 * [assign 模板参数赋值]
	 * @param  [type] $name [description]
	 * @param  [type] $val  [description]
	 * @return [type]       [description]
	 */
	public function assign($name,$val){
		static $array = array();
		$array[$name] = $val;
		$this->variables = $array;

	}
}
?>
