<?php
if(!defined("LDM_PATH")) die('No access allowed');
/* LDM 框架 应用类
* @Author: ldm
* @Date:   2014-04-30 00:56:20
* @Last Modified by:   ldm
* @Last Modified time: 2014-05-11 16:16:58
*/
class App{
	// 运行应用
	static function _app_run(){
		$control = CONTROL . 'Control' ;	//控制器文件名
		$controlFile = APP_CONTROL . $control . EXT;
		$action = ACTION;		//当前方法

		if(CONTROL == 'Index' && !is_file($controlFile)){//设置默认控制器
$str = <<<default
<?php
class IndexControl extends Control{
	function index(){
		header('Content-Type:text/html;charset=UTF-8');
		echo '<h1 style="border:1px solid #ddd;margin:20px auto;padding:20px;">欢迎使用LDM框架</h1>';
	}
}

?>
default;
		file_put_contents($controlFile, $str);
		}

		$c = new $control();	//实例化控制器
		$c->$action();			//执行当前方法
	}
	/**
	 * 应用程序初始化
	 * @return [void] [description]
	 */
	static function _init(){

		// 应用配置
		$appConfig = APP_CONFIG . 'config.php';
		is_file($appConfig) && C(require $appConfig);

		/*应用url解析并创建url常量******************************/ 
		Route::app();

		/*环境配置***************************/
		// 内存限制
		@ini_set('memory_limit', '128M');
		// 关闭全局global 变量注册
		@ini_set('register_globals', 'off');
		// 关闭自动转义
		@ini_set('magic_quotes_runtime', 0);
		// 是否开启自动转义
		define("MAGIC_QUOTES_GPC", @get_magic_quotes_gpc() ? true : false);
		// 请求方式
		define("REQUEST_METHOD", $_SERVER['REQUEST_METHOD']);
		define("IS_GET", REQUEST_METHOD == 'GET' ? true : false);
		define('IS_POST', REQUEST_METHOD == 'POST' ? true : false);
		// 是否为ajax请求
		define("IS_AJAX", is_ajax());

		// 当前时间
		define("NOW", $_SERVER['REQUEST_TIME']);
		// 当前微秒时间
		define("MICROTIME", microtime(true));
		//注册自动加载函数
		spl_autoload_register('App::autoload');	
		// 注册错误处理函数
		set_error_handler("App::error");
		// 注册异常处理函数
		set_exception_handler("App::exception");
		//是否自动开启session
		C('SESSION_START') && session_start();
	}

	/**
	 * [autoload 自动加载类函数]
	 * @param  [string] $className [类名]
	 * @return [void]            [description]
	 */
	static function autoload($className){
		$path = LDM_EXTEND_PATH . 'Tool/';
		$classFile = $path . $className . EXT;
		// echo $classFile;
		if(is_file($classFile)){//载入扩展类
			include $classFile;
		}elseif(is_file(LDM_CORE_PATH . $className . EXT)){//载入核心类
			include LDM_CORE_PATH . $className . EXT;
		}elseif(strpos($className, 'Control')){//载入控制器
			$classFile = APP_PATH . 'Control/' . $className . EXT;
			is_file($classFile) && include_once($classFile);
		}elseif(strpos($className, 'Model')){//载入模型类
			$classFile = APP_PATH . 'Model/' . $className . EXT;

		}
	}

	/**
	 * [error 错误处理函数]
	 * @param  [type] $errNo   [错误的级别]
	 * @param  [type] $errStr  [错误信息]
	 * @param  [type] $errFile [错误文件]
	 * @param  [type] $errLine [错误行号]
	 * @return [type]          [description]
	 */
	static function error($errNo,$errStr,$errFile,$errLine){
		// 错误信息
		$msg = '';
		$msg .= '[TIME]' . date('Y-m-d H:i:s');
		$msg .= '[ERROR]' . $errStr;
		$msg .= '[FILE]' . $errFile;
		$msg .= '[LINE]' . $errLine . "\n";

		// 判断错误级别
		switch ($errNo) {
			case E_ERROR:
			case E_PARSE:
			case E_USER_ERROR://致命的用户生成的错误
				//Log::write($msg);	//记录错误日志
				error($msg);		//显示错误
			break;
			case E_USER_WARNING:	//非致命用户错误
			case E_USER_NOTICE:		//用户生成的通知
			default:				//提示性错误
				Log::set($msg);
				if(DEBUG || C('DEBUG')){
					include is_file(C('NOTICE_TPL')) ? C('NOTICE_TPL') : TPL_PATH . 'notice.html';
				}

				break;
		}

	}

	/**
	 * [_exception 异常错误]
	 * @param  [type] $msg [异常信息]
	 * @return [type]      [description]
	 */
	static function exception($msg){
		$e = array();	//异常信息

		$e['message'] = $msg->getMessage();
		$trace = $msg->getTrace();	//获取异常追踪信息
		// $e['trace'] = $trace;
		if($trace[0]['function'] == 'throw_exception'){
			$e['file'] = $trace[0]['file'];
			$e['line'] = $trace[0]['line'];
		}else{
			$e['file'] = $msg->getFile();
			$e['line'] = $msg->getLine();
		}

		Log::write($e['message']);

		error($e);
		
	}
}
?>
