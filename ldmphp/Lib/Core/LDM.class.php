<?php
if(!defined("LDM_PATH")) die('No access allowed');
/* ldmphp 框架  核心 LDM 类
* @Author: ldm
* @Date:   2014-04-28 10:17:16
* @Last Modified by:   ldm
* @Last Modified time: 2014-05-11 16:17:51
*/
final class LDM{

	private static $_compile;	//编译内容

	static function run(){
		// 1 定义框架所需常量
		self::_define_const();
		

		// 2 载入核心文件
		self::_load_core_files();
		// 3 系统配置项
		C(require LDM_CONFIG_PATH . 'config.php');

		// 6 组合编译文件,自动加载的文件，并生成编译文件
		self::_create_compile_file();

		// 4 环境配置：包括：
		// 应用程序初始化,定义请求方式，注册自动载入类，异常处理，错误处理
		// 应用url解析并配置常量，加载应用配置，session开启 等
		App::_init();

		// 5 创建应用文件夹
		self::_mk_dirs();
		
		

		// 7 运行应用
		App::_app_run();
	}

	/**
	 * [_load_core_files 载入核心文件]
	 * @return [type] [description]
	 */
	private static function _load_core_files(){
		// 存放核心编译文件
		$files = require LDM_COMPILE_PATH . 'compile.php';
		foreach ($files as $v) {
			require $v;
		}
	}

	/**
	 * [_create_compile_file 生成编译文件]
	 * @return [type] [description]
	 */
	private static function _create_compile_file(){
		// 编译文件
		$compileFile = TEMP_PATH . TEMP_FILE;
		// 开启调试，删除编译文件
		if(DEBUG){
			is_file($compileFile) && unlink($compileFile);
			return true;
		}

		// 编译内容
		$_compile = '';
		// 1 常量编译
		$const = get_defined_constants(true);
		foreach ($const['user'] as $d => $v) {
			if($v=='\\'):
				$v = "'\\\\'";
			else:
				$v = is_int($v) ? (int)$v : "'{$v}'";
			endif;
			$_compile .= "defined('{$d}') OR define('{$d}',{$v});";

		}
		
		// 2 核心文件编译
		 $_coreFiles = include LDM_COMPILE_PATH. 'compile.php';
		 foreach ($_coreFiles as $v) {
		 	$content = compress(trim(file_get_contents($v)));
		 	$_compile .= substr($content, -2) == '?>' ? trim(substr($content,5,-2)) : trim(substr($content,5));
		 }
		 // 3 框架配置项
		 $_compile .= 'C(' . var_export(C(),true) . ');';

		 // 4 自动加载文件
		 $autoFiles = preg_split('#,#i', C('AUTO_LOAD_FILE'));
		 if(!empty($autoFiles)){
		 	foreach ($autoFiles as $v) {
		 		$file = preg_replace('#\.php#i', '', $v).'.php';
		 		if(strpos($file, '/')===false){
		 			$file = APP_LIB . $file;
		 		}
		 		if(is_file($file)){
			 		require_cache($file);	
			 		$content = compress(trim(file_get_contents($file)));
			 		$_compile .= substr($content,-2) == '?>' ? trim(substr($content,5,-2)) : trim(substr($content,5));
		 		}

		 	}

		 }

		 // 5 应用启用编译
		 $_compile .= 'App::_init();App::_app_run();';

		 self::$_compile = $_compile;


		 // 写入编译文件
		 @file_put_contents($compileFile, "<?php " . self::$_compile);
	}

	/**
	 * [_define_const 定义框架所需常量常量]
	 * @return [type] [description]
	 */
	private static function _define_const(){
		// 定义系统目录常量
		//网站公共目录**************
		defined("PUBLIC_PATH") 	|| define("PUBLIC_PATH", ROOT_PATH . 'Public/');
		//类库目录 LIB
		defined("LDM_LIB_PATH") 	|| define("LDM_LIB_PATH", LDM_PATH . 'Lib/');
		// 系统模版目录
		defined("LDM_TPL_PATH")		|| define("LDM_TPL_PATH", LDM_LIB_PATH . 'Tpl/');
		// 编译文件定义目录
		defined("LDM_COMPILE_PATH")	|| define("LDM_COMPILE_PATH", LDM_LIB_PATH . 'Compile/');
		//框架配置文件目录
		defined("LDM_CONFIG_PATH") 	|| define("LDM_CONFIG_PATH", LDM_PATH . 'Config/');	
		//框架数据目录
		defined("LDM_DATA_PATH")	|| define("LDM_DATA_PATH", LDM_PATH . 'Data/');	
		//ldm框架核心类目录
		defined("LDM_CORE_PATH") 	|| define("LDM_CORE_PATH",  LDM_LIB_PATH . 'Core/');	
		//核心函数目录
		defined("LDM_FUNC_PATH")	|| define("LDM_FUNC_PATH", LDM_LIB_PATH . 'Function/');	
		//系统扩展目录
		defined('LDM_EXTEND_PATH')	|| define("LDM_EXTEND_PATH", LDM_PATH . 'Extend/');	

		/*定义项目目录*****************************************/
		defined('APP_CONTROL')	|| define("APP_CONTROL", APP_PATH . 'Control/');		//应用控制器目录
		defined('APP_MODEL')	|| define("APP_MODEL", APP_PATH . 'Model/');			//应用模型目录
		defined('APP_CONFIG')	|| define("APP_CONFIG", APP_PATH . 'Config/');			//应用配置项目录
		defined('APP_TPL')	|| define("APP_TPL", APP_PATH . 'Tpl/');				//应用模板目录
		defined('APP_TPL_PUBLIC')	|| define("APP_TPL_PUBLIC", APP_TPL . 'Public/');		//应用模板公共目录
		defined('APP_EXTEND')	|| define("APP_EXTEND", APP_PATH . 'Extend/');			//应用扩展类目录
		defined('APP_LOG')	|| define("APP_LOG", TEMP_PATH . 'Log/');				//应用日志目录
		defined('APP_LIB')	|| define("APP_LIB", APP_EXTEND . 'Lib/');				//应用日志目录
	}
	/**
	 * [_mk_dirs 常见应用文件夹]
	 * @return [type] [description]
	 */
	private static function _mk_dirs(){
		/*项目目录*/
		$appDirs = array(
			APP_CONTROL,
			APP_MODEL,
			APP_CONFIG,
			APP_TPL,
			APP_TPL_PUBLIC,
			APP_EXTEND,
			);
		// 创建应用目录
		foreach($appDirs as $v){
			is_dir($v) || mkdir($v,0777,true);
		}
		// 生成项目配置文件
		$config = APP_CONFIG . 'config.php';
		if(!is_file($config)){
			file_put_contents($config, "<?php\r\nreturn array();\r\n?>");
		}
	
	}
}