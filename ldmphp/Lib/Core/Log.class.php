<?php
/* LDM 框架 日志类
* @Author: dm
* @Date:   2014-04-30 15:49:35
* @Last Modified by:   ldm
* @Last Modified time: 2014-04-30 22:09:26
*/
class Log{
	static $log = array();	//日志文件

	/**
	 * [set 记录日志内容(保存到类属性-用于调试显示)]
	 * @param [type] $msg [description]
	 */
	public static function set($msg){
		if(DEBUG) return;
		self::$log[] = $msg . '[TIME]' . date("Y-m-d H:i:s") ;
	}
	/**
	 * [write 写入日志]
	 * @param  [type]  $msg         [日志信息]
	 * @param  integer $type        [处理方式，3为保存到指定文件]
	 * @param  [type]  $destination [日志文件]
	 * @param  [type]  $extraHeader [description]
	 * @return [type]               [description]
	 */
	public static function write($msg,$type=3,$destination=null,$extraHeader=null){
		if(C('DEBUG') || !C('LOG_ON')){//调试模式或设置了不保存日志
			return;
		}
		
		// 日志文件
		$destination = is_null($destination) ? self::_get_log_file() : $destination;
		// 创建目录
		is_dir(dirname($destination)) || mkdir(dirname($destination),0777,true);

		$msg = $msg . "\t时间:" . date('Y-M-d H:i:s') . "\r\n";
		error_log($msg,$type,$destination,$extraHeader);
	}


	/**
	 * [_get_log_file 获得日志文件]
	 * @return [type] [description]
	 */
	private static function _get_log_file(){
		$name = md5_d(C('LOG_KEY'));
		$file = APP_LOG . $name . '.log';
		

		if(is_file($file) && filesize($file) > C('LOG_SIZE')){//如果日志文件大于规定大小，重命名当前日志文件
			$newFile = APP_LOG . $name . '_' .date('Y-m-d H:i:s') .'.log';
			rename($file, $newFile);
		}
		return  $file;
	}



}