<?php


/**
 * [p 打印]
 * @param  [type] $arr [description]
 * @return [type]      [description]
 */
function p($arr){
	echo '<pre>';
	print_r($arr);
	echo "</pre>";
}

/**
 * [success 成功提示]
 * @param  string $msg [description]
 * @param  string $url [description]
 * @return [type]      [description]
 */
function success($msg='操作成功！',$url=''){
	header("Content-Type:text/html;charset=utf-8");
	$url = $url?$url:__ROOT__;
	echo "<script>alert('".$msg."');location.href='".$url."';</script>";
	exit;
}


/**
 * 获取或设置配置项
 * @param [type] $name  [description]
 * @param [type] $value [description]
 */
function C($name=null,$value=null){
	static $config_arr = array();
	if(is_null($name)){//没有参数，返回所有已加载配置项
		return $config_arr;
	}
	// 传入两个参数，进行配置项设置（只是加入当前加载项）
	if($value){
		if(is_array($name)){
			array_merge($config_arr,$name);
		}
		if(is_string($name)){
			$config_arr[$name] = $value;
		}
		return;
	}

	if($name){
		if(is_array($name)){
			$config_arr = array_merge($config_arr,$name);
			return;
		}
		if(isset($config_arr[$name])){
			return $config_arr[$name];
		}
	}

}

/**
 * [compress 去空格，去除注释包括单行及多行注释]
 * @param  [type] $con [需要处理内容]
 * @return [string]      [处理后内容]
 */
function compress($content){
    $str = ""; //合并后的字符串
    $data = token_get_all($content);
    $end = false; //没结束如$v = "hdphp"中的等号;
    for ($i = 0, $count = count($data); $i < $count; $i++) {
        if (is_string($data[$i])) {
            $end = false;
            $str .= $data[$i];
        } else {
            switch ($data[$i][0]) { //检测类型
                //忽略单行多行注释
                case T_COMMENT:
                case T_DOC_COMMENT:
                    break;
                //去除格
                case T_WHITESPACE:
                    if (!$end) {
                        $end = true;
                        $str .= " ";
                    }
                    break;
                //定界符开始
                case T_START_HEREDOC:
                    $str .= "<<<LDMPHP\n";
                    break;
                //定界符结束
                case T_END_HEREDOC:
                    $str .= "LDMPHP;\n";
                    //类似str;分号前换行情况
                    for ($m = $i + 1; $m < $count; $m++) {
                        if (is_string($data[$m]) && $data[$m] == ';') {
                            $i = $m;
                            break;
                        }
                        if ($data[$m] == T_CLOSE_TAG) {
                            break;
                        }
                    }
                    break;

                default:
                    $end = false;
                    $str .= $data[$i][1];
            }
        }
    }
    return $str;
}
/**
 * [md5_d 加密字符串]
 * @param  [type] $str [description]
 * @return [string]      [加密后字符串]
 */
function md5_d($str){
    return !empty($str) ? md5(serialize($str)) : '';
}
/**
 * [is_ajax 判断是否为ajax请求]
 * @return boolean []
 */
function is_ajax(){
    if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        return true;
    return false;
}

/**
 * [error 输出错误信息]
 * @param  [type] $msg [错误信息内容]
 * @return [type]      [description]
 */
function error($error){

	if(DEBUG || C('DEBUG')){//开启调试,直接显示调试信息
		$e = array();
		if(!is_array($error)){
			$trace = debug_backtrace();	//产生一条回溯跟踪
			$e['message'] = $error;
			$e['file'] = $trace[0]['file'];
			$e['line'] = $trace[0]['line'];
			$e['class'] = isset($trace[0]['class']) ? $trace[0]['class'] :'';
			$e['function'] = isset($trace[0]['function']) ? $trace[0]['function'] :'';
			ob_start();
			debug_print_backtrace();
			$trace = ob_get_clean();
			$e['trace'] = $trace;
		}else{
			$e = $error;
		}


	}else{//没有开启调试，保存错误信息
		Log::write($error);	//记录错误日志

		$e['message'] = "出错啦，开启DEBUG或查看log文件";
	}
	// var_dump($e);die;
	if(is_file(C('ERROR_TPL'))){
		include C('ERROR_TPL');
	}else{
		include TPL_PATH . 'halt.html';
	}
	exit;
}
/**
 * 返回错误类型
 * @param int $type
 * @return strings
 */
function FriendlyErrorType($type)
{
    switch ($type) {
        case E_ERROR: // 1 //
            return 'E_ERROR';
        case E_WARNING: // 2 //
            return 'E_WARNING';
        case E_PARSE: // 4 //
            return 'E_PARSE';
        case E_NOTICE: // 8 //
            return 'E_NOTICE';
        case E_CORE_ERROR: // 16 //
            return 'E_CORE_ERROR';
        case E_CORE_WARNING: // 32 //
            return 'E_CORE_WARNING';
        case E_CORE_ERROR: // 64 //
            return 'E_COMPILE_ERROR';
        case E_CORE_WARNING: // 128 //
            return 'E_COMPILE_WARNING';
        case E_USER_ERROR: // 256 //
            return 'E_USER_ERROR';
        case E_USER_WARNING: // 512 //
            return 'E_USER_WARNING';
        case E_USER_NOTICE: // 1024 //
            return 'E_USER_NOTICE';
        case E_STRICT: // 2048 //
            return 'E_STRICT';
        case E_RECOVERABLE_ERROR: // 4096 //
            return 'E_RECOVERABLE_ERROR';
        case E_DEPRECATED: // 8192 //
            return 'E_DEPRECATED';
        case E_USER_DEPRECATED: // 16384 //
            return 'E_USER_DEPRECATED';
    }
    return $type;
}

/**
 * [require_cache 加载文件并缓存]
 * @param  [type] $file [加载的文件]
 * @return [type]       [description]
 */
function require_cache($file=null){
    static $_require_files = array();
    if(is_null($file)) return $_require_files;

    $name = strtolower($file);
    if(isset($_require_files[$name])){
        // 已加载文件，返回
        return true;
    }
    // 文件不存在，返回false
    if(!file_exists($file) && !file_exists($name)){
        return false;
    }
    // 加载文件
    $_require_files[$name] = true;
    require $name;
    return true;
}

/**
 * [M 实例化模型]
 * @param [type] $table [description]
 */
function M($table){
	return new Db($table);
}