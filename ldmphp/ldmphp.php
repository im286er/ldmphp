<?php
/* ldm框架 框架入口文件
* @Author: ldm
* @Date:   2014-04-28 00:54:23
* @Last Modified by:   ldm
* @Last Modified time: 2014-05-11 16:18:30
*/
//类文件后缀
const EXT = '.class.php';		
//框架目录
define("LDM_PATH", str_replace('\\','/',__DIR__).'/');				
//网站根目录
define("ROOT_PATH",  str_replace('\\','/',dirname($_SERVER['SCRIPT_FILENAME'])).'/');	
//项目目录
defined("APP_PATH")		|| define('APP_PATH', ROOT_PATH.'APP/');
// 临时文件目录
defined("TEMP_PATH") 	|| define("TEMP_PATH", APP_PATH.'Temp/');		
//是否开启调试
defined("DEBUG") 		|| define('DEBUG', false);		
// 编译文件
$complie_file =  '~Boot.php';	
//编译文件							
define("TEMP_FILE", $complie_file);		

if(is_file(TEMP_PATH . TEMP_FILE) && !DEBUG){//存在编译文件，并且没有开启调试
	require TEMP_PATH . TEMP_FILE;
}else{//进行编译处理
	require LDM_PATH . 'Lib/Core/LDM' . EXT;
	//运行框架
	LDM::run();	
}